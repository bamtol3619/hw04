#include <iostream>
#include <stdlib.h>

//2015004057 김범수

using namespace std;

int BlackJack(string* Cards,int number);

int main(){
	int number=1;
	while(number){
		cin >> number;
		if(number<=0)	break;
		string* Cards = new string[number];
		for(int i=0;i<number;++i)	cin >> Cards[i];
		int result=BlackJack(Cards,number);
		if(result>0 && result<21)	cout << result << endl;
		else if(result==21)	cout << "BlackJack" << endl;
		else if(result==0)	cout << "Exceed" << endl;
		else if(result==-1)	break;
		delete[] Cards;
	}
	return 0;
}

int BlackJack(string* Cards,int number){
	for(int i=0;i<number;++i)	if(Cards[i]!="A" && Cards[i]!="K" && Cards[i]!="Q" && Cards[i]!="J" && (atoi(Cards[i].c_str())>10 || atoi(Cards[i].c_str())<2))	return -1;	//카드 입력이 올바른지 확인.
	bool A_used_once = false;
	int total=0;
	for(int i=0;i<number;++i){
		if(Cards[i]=="K" || Cards[i]=="Q" || Cards[i]=="J")	total+=10;	//KQJ의 경우 10을 더하고
		else if(A_used_once==true && Cards[i]=="A")	total+=1;	//A가 2번 이상 쓰인경우엔 1로 생각한다 
		else if(A_used_once==false && Cards[i]=="A")	A_used_once=true;	//A가 처음 쓰인것을 확인하고 
		else	total+=atoi(Cards[i].c_str());	//알파벳을 제외한 경우엔 숫자를 더한다 
	}
	if(A_used_once){	//A가 쓰인 상황에서 상황에 맞게 1또는11을 더한다 
		if(total+11<=21)	total+=11;
		else if(total+1<=21)	total+=1;
	}
	if(total<=21)	return total;
	else	return 0;
}
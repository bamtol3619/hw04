#include "minesweeper.h"
#include <iostream>
#include <vector>
#include <string>

using namespace std;

Minesweeper::Minesweeper(){
	vector<string> Map;
	vector<string> Printing;
	vector<string> Playing;
	int col=0;
	int row=0;
	int counting=0;
	bool is_playing_game=false;
}

Minesweeper::~Minesweeper(){}

bool Minesweeper::setMap(int _width, int _height){
	if(_width<1 || _height<1)	return false;	//incorrect input
	else{
		is_playing_game=false;
		Map.clear();
		col=_width;
		row=_height;
		for(int i=0;i<_height;++i){
			string mapping;
			getline(cin, mapping);
			if(mapping.length()!=_width)	return false;	//incorrect input
			for(int j=0;j<_width;++j){
				char c=mapping[j];
				if(c!='.' && c!='*')	return false;	//incorrect input
			}
			Map.push_back(mapping);
		}
	printMap(false);
	return true;
	}
}

bool Minesweeper::toggleMine(int _x, int _y){
	if(get(_x, _y)==' ')	return false;	//incorrect input
	else if(is_playing_game)	return false;
	else{
		if(get(_x, _y)=='.')	Map[_y][_x]='*';
		else if(get(_x, _y)=='*')	Map[_y][_x]='.';
		printMap(false);
		return true;
	}
}

int Minesweeper::width() const{return col;}

int Minesweeper::height() const{return row;}

char Minesweeper::get(int _x, int _y) const{
	if(_x<0 || _x>=col || _y<0 || _y>=row)	return ' ';
	else 	return Map[_y][_x];
}

bool Minesweeper::setPlay(){
	if(col!=0 && row!=0){
		Playing.clear();
		counting=0;
		is_playing_game=true;
		for(int i=0;i<row;++i){
			string mapping;
			for(int j=0;j<col;++j){
				mapping.push_back('_');
			}
			Playing.push_back(mapping);
		}
		printMap(true);
		return true;
	}
	else	return false;
}

bool Minesweeper::touchMap(int _x, int _y){
	++counting;
	if(get(_x, _y)=='*'){
		is_playing_game=false;
		return true;
	}
	else{
		Playing[_y][_x]=Printing[_y][_x];
		printMap(true);
		return false;
	}
}

int Minesweeper::touchCount() const{return counting;}

void Minesweeper::printMap(bool playing_game){
	if(playing_game){	//:play이후 보여줄 지도
		for(int i=0;i<row;++i)	cout << Playing[i] << endl;
	}
	else{	//:set :toggle 이후 보여줄 지도
		Printing.clear();
		for(int i=0;i<row;++i){
			string numbers;
			for(int j=0;j<col;++j){
				int minenumber=0;
				if(Map[i][j]=='*'){
					numbers.push_back('*');
					continue;
				}
				else if(i==0 && j==0){	//왼쪽위구석의 경우
					if(Map[i+1][j]=='*')	++minenumber;
					if(Map[i+1][j+1]=='*')	++minenumber;
					if(Map[i][j+1]=='*')	++minenumber;
				}
				else if(i==0 && j==col-1){	//오른쪽위구석의 경우
					if(Map[i][j-1]=='*')	++minenumber;
					if(Map[i+1][j-1]=='*')	++minenumber;
					if(Map[i+1][j]=='*')	++minenumber;
				}
				else if(i==row-1 && j==0){	//왼쪽아래구석의 경우
					if(Map[i-1][j]=='*')	++minenumber;
					if(Map[i-1][j+1]=='*')	++minenumber;
					if(Map[i][j+1]=='*')	++minenumber;
				}
				else if(i==row-1 && j==col-1){	//오른쪽아래구석의 경우
					if(Map[i-1][j]=='*')	++minenumber;
					if(Map[i-1][j-1]=='*')	++minenumber;
					if(Map[i][j-1]=='*')	++minenumber;
				}
				else if(i==0){	//왼쪽의 경우
					if(Map[i-1][j]=='*')	++minenumber;
					if(Map[i-1][j+1]=='*')	++minenumber;
					if(Map[i][j+1]=='*')	++minenumber;
					if(Map[i+1][j+1]=='*')	++minenumber;
					if(Map[i+1][j]=='*')	++minenumber;
				}
				else if(j==col-1){	//오른쪽의 경우
					if(Map[i-1][j]=='*')	++minenumber;
					if(Map[i-1][j-1]=='*')	++minenumber;
					if(Map[i][j-1]=='*')	++minenumber;
					if(Map[i+1][j-1]=='*')	++minenumber;
					if(Map[i+1][j]=='*')	++minenumber;
				}
				else if(i==row-1){	//아래쪽의 경우
					if(Map[i][j+1]=='*')	++minenumber;
					if(Map[i-1][j+1]=='*')	++minenumber;
					if(Map[i-1][j]=='*')	++minenumber;
					if(Map[i-1][j-1]=='*')	++minenumber;
					if(Map[i][j-1]=='*')	++minenumber;
				}
				else{	//그외의 경우
					if(Map[i-1][j]=='*')	++minenumber;
					if(Map[i-1][j-1]=='*')	++minenumber;
					if(Map[i][j-1]=='*')	++minenumber;
					if(Map[i+1][j-1]=='*')	++minenumber;
					if(Map[i+1][j]=='*')	++minenumber;
					if(Map[i+1][j+1]=='*')	++minenumber;
					if(Map[i][j+1]=='*')	++minenumber;
					if(Map[i-1][j+1]=='*')	++minenumber;
				}
				numbers.push_back(char(48+minenumber));
			}
			Printing.push_back(numbers);
		}
		for(int i=0;i<row;++i)	cout << Printing[i] << endl;
	}
}
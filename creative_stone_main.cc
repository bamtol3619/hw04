#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <map>
#include "creative_stone.h"

using namespace std;


void Print_minions(map<string,MinimalFighter> _Ally,map<string,MinimalFighter> _Enemy){
	map<string,MinimalFighter>::iterator ally_it = _Ally.begin();
	map<string,MinimalFighter>::iterator enemy_it = _Enemy.begin();
	while(ally_it!=_Ally.end() || enemy_it!=_Enemy.end()){
		if(ally_it!=_Ally.end()){
			cout << (ally_it->second).name() << " " << (ally_it->second).hp() << " " << (ally_it->second).power();
			++ally_it;
		}
		if(enemy_it!=_Enemy.end()){
			cout << " / ";
			cout << (enemy_it->second).name() << " " << (enemy_it->second).hp() << " " << (enemy_it->second).power();
			++enemy_it;
		}
		cout << endl;
	}	
}
bool processFight(map<string,MinimalFighter>& _Ally, map<string,MinimalFighter>& _Enemy){
    string inputs;
    getline(cin,inputs);
    
    if(inputs.find(":quit")!= string::npos)	return false;
    
    else if(inputs.find(":add")!= string::npos){	//명령어 :add 
    	inputs.erase(0, 5);
    	string ally_name=inputs.substr(0,inputs.find(" "));
    	inputs.erase(0,inputs.find(" ")+1);
    	int ally_hp=atoi(inputs.substr(0,inputs.find(" ")).c_str());
    	int ally_power=atoi(inputs.substr(inputs.find(" ")+1,inputs.length()-inputs.find(" ")-1).c_str());
		
		if(_Ally.find(ally_name) == _Ally.end()){	//:add에서 처음나온 이름으로 생성할때 
			if(ally_hp>0){
				MinimalFighter temp(ally_name, ally_hp, ally_power);
	    		_Ally.insert(make_pair(ally_name,temp));
	    	}
		}
		else{	//이미 같은이름이 있는경우 
			_Ally[ally_name].setHp(_Ally[ally_name].hp()+ally_hp);
			_Ally[ally_name].setPower(_Ally[ally_name].power()+ally_power);
			if(_Ally[ally_name].hp()<=0){	//능력치 변화후 체력이 0이하이면 파괴 
				_Ally.erase(ally_name);
			}
		}
    	Print_minions(_Ally,_Enemy);
    	return true;
	}
	
	else if(inputs.find(":foeadd")!= string::npos){	//명령어 :foeadd 
		inputs.erase(0, 8);
    	string enemy_name=inputs.substr(0,inputs.find(" "));
    	inputs.erase(0,inputs.find(" ")+1);
    	int enemy_hp=atoi(inputs.substr(0,inputs.find(" ")).c_str());
    	int enemy_power=atoi(inputs.substr(inputs.find(" ")+1,inputs.length()-inputs.find(" ")-1).c_str());
		
		if(_Enemy.find(enemy_name) == _Enemy.end()){	//:foeadd에서 처음나온 이름으로 생성할때 
			if(enemy_hp>0){
				MinimalFighter temp(enemy_name, enemy_hp, enemy_power);
	    		_Enemy.insert(make_pair(enemy_name,temp));
	    	}
		}
		else{	//이미 같은이름이 있는경우 
			_Enemy[enemy_name].setHp(_Enemy[enemy_name].hp()+enemy_hp);
			_Enemy[enemy_name].setPower(_Enemy[enemy_name].hp()+enemy_power);
			if(_Enemy[enemy_name].hp()<=0){	//능력치 변화후 체력이 0이하이면 파괴 
				_Enemy.erase(enemy_name);
			}
		}
    	Print_minions(_Ally,_Enemy);
    	return true;
	}
	
	else if(inputs.find(":attack")!= string::npos){	//명령어 :attack 
		inputs.erase(0, 8);
		string ally_name=inputs.substr(0,inputs.find(" "));
		string enemy_name=inputs.substr(inputs.find(" ")+1,inputs.length()-inputs.find(" ")-1);
		if(_Ally.find(ally_name)==_Ally.end() || _Enemy.find(enemy_name)==_Enemy.end()){
			cout << "CANNOT FIND MINION" << endl;
		}
		else{
			int ally_hp=_Ally[ally_name].hp(),ally_power=_Ally[ally_name].power(),enemy_hp=_Enemy[enemy_name].hp(),enemy_power=_Enemy[enemy_name].power();
			if(ally_hp-enemy_power<=0)	_Ally.erase(ally_name);
			else	_Ally[ally_name].setHp(ally_hp-enemy_power);
			if(enemy_hp-ally_power<=0)	_Enemy.erase(enemy_name);
			else	_Enemy[enemy_name].setHp(enemy_hp-ally_power);
			Print_minions(_Ally,_Enemy);
		}
		return true;
	}
	
	else if(inputs.find(":burn")!= string::npos){	//명령어 :burn 
		inputs.erase(0, 6);
		int burning_power=0;
		if(inputs.find(" ")==string::npos){	//:burn #
			bool input_correct=true;
			for(int i=0;i<inputs.length();++i){
				if(isdigit(inputs[i])==false){
					input_correct=false;
					break;
				}	
			}
			if(input_correct){
				burning_power=atoi(inputs.c_str());
				map<string,MinimalFighter>::iterator it=_Enemy.begin();
				while(it!=_Enemy.end()){
					if((it->second).hp()-burning_power<=0)	_Enemy.erase(it++);
					else{
						(it->second).setHp((it->second).hp()-burning_power);
						++it;
					}
				}
				Print_minions(_Ally,_Enemy);
			}
		}
		else{	//:burn name #
			string minion_name=inputs.substr(0,inputs.find(" "));
			burning_power=atoi(inputs.substr(inputs.find(" ")+1,inputs.length()-inputs.find(" ")-1).c_str());
			if(_Enemy.find(minion_name)!=_Enemy.end()){
				if(_Enemy[minion_name].hp()-burning_power<=0)	_Enemy.erase(minion_name);
				else	_Enemy[minion_name].setHp(_Enemy[minion_name].hp()-burning_power);
				Print_minions(_Ally,_Enemy);
			}
			else if(_Ally.find(minion_name)!=_Ally.end()){
				if(_Ally[minion_name].hp()-burning_power<=0)	_Ally.erase(minion_name);
				else	_Ally[minion_name].setHp(_Ally[minion_name].hp()-burning_power);
				Print_minions(_Ally,_Enemy);
			}
			else	cout << "CANNOT FIND MINION" << endl;
		}
		return true;
	}
	return false;
}

int main(void){
	map<string,MinimalFighter> Ally;
	map<string,MinimalFighter> Enemy;
	
    while(processFight(Ally,Enemy)){ /* Nothing else to do. */ }
    return 0;
}
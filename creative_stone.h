// creative_stone.h
//2015004057 김범수

#ifndef __hw04__creative_stone__
#define __hw04__creative_stone__

#include <iostream>

using namespace std;

class MinimalFighter{
	private:
		string mName;
		int mHp;
		int mPower;

	public:
		MinimalFighter();
		MinimalFighter(string _name, int _hp, int _power);
		~MinimalFighter(){}

		string name() const{return mName;}
		int hp() const{return mHp;}
		int power() const{return mPower;}

		void setHp(int _hp);
		void setPower(int _power);

		void hit(MinimalFighter *_enemy);
		void attack(MinimalFighter *_target);
		void fight(MinimalFighter *_enemy);
};

#endif
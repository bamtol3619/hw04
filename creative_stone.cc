#include <iostream>
#include "creative_stone.h"

using namespace std;

MinimalFighter::MinimalFighter(){
	mName="";
	mHp=0;
	mPower=0;
}

MinimalFighter::MinimalFighter(string _name, int _hp, int _power){
	mName=_name;
	mHp=_hp;
	mPower=_power;
}

void MinimalFighter::setHp(int _hp){
	mHp=_hp;
}

void MinimalFighter::setPower(int _power){
	mPower=_power;	
}
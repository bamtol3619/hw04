#ifndef minesweeper_h
#define minesweeper_h

#include <stdio.h>
#include <string>
#include <vector>
#include <sstream>
#include <iostream>

using namespace std;

class Minesweeper
{
public:
    ////////// 4-3-1 (7 score) //////////
    Minesweeper();
    ~Minesweeper();
    
    // return false when input is incorrect
    bool setMap(int _width, int _height);
    bool toggleMine(int _x, int _y);
    
    // return map width, height, and char
    int width() const;
    int height() const;
    char get(int _x, int _y) const; // return ' ' if input is illegal
    
    ////////// 4-3-2 (3 score) //////////
    bool setPlay(); // return false when map is not set
    bool touchMap(int _x, int _y); // return true when dead
    
    int touchCount() const;
    
    bool is_playing_game;
private:
	vector<string> Map;
	vector<string> Printing;
	vector<string> Playing;
	int col;
	int row;
	int counting;
	void printMap(bool playing_game);
    // add private member variables and functions if it is needed.
};

#endif /* minesweeper_h */
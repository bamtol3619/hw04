#include <iostream>
#include <vector>
#include <string>
#include <stdlib.h>
#include "minesweeper.h"

//2015004057 김범수

using namespace std;

bool MineOperation(Minesweeper* _minesweeper){
	string inputs;
	getline(cin, inputs);

	if(inputs.find(":quit")!= string::npos)	return false;

	else if(inputs.find(":set")!= string::npos){
		inputs.erase(0, 5);
		int x, y;
		x=atoi(inputs.substr(0, inputs.find(" ")).c_str());
		y=atoi(inputs.substr(inputs.find(" ")+1, inputs.length()-inputs.find(" ")-1).c_str());
		if(_minesweeper->setMap(x, y))	return true;
	}

	else if(inputs.find(":toggle")!= string::npos){
		inputs.erase(0, 8);
		int x, y;
		x=atoi(inputs.substr(0, inputs.find(" ")).c_str());
		y=atoi(inputs.substr(inputs.find(" ")+1, inputs.length()-inputs.find(" ")-1).c_str());
		if(_minesweeper->toggleMine(x, y))	return true;
	}

	else if(inputs.find(":play")!= string::npos && _minesweeper->is_playing_game){
		if(_minesweeper->setPlay())	return true;
	}

	else if(inputs.find(":touch")!= string::npos && _minesweeper->is_playing_game){
		inputs.erase(0, 7);
		int x, y;
		x=atoi(inputs.substr(0, inputs.find(" ")).c_str());
		y=atoi(inputs.substr(inputs.find(" ")+1, inputs.length()-inputs.find(" ")-1).c_str());
		if(_minesweeper->get(x, y)==' ')	return false;	//incorrect input
		else if(_minesweeper->touchMap(x, y))	cout << "DEAD(" << _minesweeper->touchCount() << ")" << endl;
		else return true;
	}
	return false;	//incorrect input
}

int main(){
	Minesweeper* minesweeper = new Minesweeper;

	while(MineOperation(minesweeper)){}

	delete minesweeper;
	return 0;
}
#include <iostream>
#include "reply_admin.h"

ReplyAdmin::ReplyAdmin(){
	list<string> chats;
	count=0;
	addChat("Hello, Reply Administrator!");
	addChat("I will be a good programmer.");
	addChat("This class is awesome.");
	addChat("Professor Lim is wise.");
	addChat("Two TAs are kind and helpful.");
	addChat("I think male TA looks cool.");
}

ReplyAdmin::~ReplyAdmin(){}

bool ReplyAdmin::addChat(string _chat){
	if(count>=NUM_OF_CHAT)	return false;	//댓글이 최대 갯수인 NUM_OF_CHAT(200)을 초과한다면 add는 실패
	++count;
	chats.push_back(_chat);
	return true;
}

bool ReplyAdmin::removeChat(int _index){
	if(_index>=count)	return false;	//숫자가 댓글에 없는 번호라면 remove는 실패
	list<string>::iterator it=chats.begin();
	for(int i=0;i<_index;++i)	++it;
	it=chats.erase(it);
	--count;
	return true;
}

bool ReplyAdmin::removeChat(int *_indices, int _count){
	bool checking=false;
	int temp=0;
	for(int i=0;i<_count;++i){	//배열에 있는 숫자가 모두 다 댓글에 없는 번호들이라면 remove는 실패
		if(_indices[i]-temp<count){
			list<string>::iterator it=chats.begin();
			for(int j=0;j<_indices[i]-temp;++j)	++it;
			chats.erase(it);
			++temp;
			--count;
			checking=true;
		}
	}
	return checking;
}

bool ReplyAdmin::removeChat(int _start, int _end){
	if(_start>=count)	return false;	//스타트가 댓글에 없는 번호라면 remove는 실패
	int ending=_end;
	if(_end>count)	ending=count-1;
	list<string>::iterator it = chats.begin();
	for(int i=0;i<_start;++i)	++it;	
	for(int i=0;i<ending-_start+1;++i){
		it=chats.erase(it);
		--count;
	}
	return true;
}

void ReplyAdmin::printChat(){
	int i=0;
	for(list<string>::iterator it=chats.begin();it!=chats.end();++it)	cout << i++ << " " << *it << endl;
}